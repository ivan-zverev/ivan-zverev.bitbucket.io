﻿declare var $: any;

class Roller {
    rollGrey(): RollResult
    {
        var randomNumber = Math.floor(Math.random() * 6) + 1;
        return randomNumber == 6 ? RollResult.Critical : randomNumber == 5 ? RollResult.Hit : RollResult.Miss;
    }

    rollPurple(): RollResult {
        var randomNumber = Math.floor(Math.random() * 8) + 1;
        return randomNumber >= 7 ? RollResult.Critical : randomNumber >= 5 ? RollResult.Hit : RollResult.Miss;
    }

    rollBlue(): RollResult {
        var randomNumber = Math.floor(Math.random() * 12) + 1;
        return randomNumber >= 9 ? RollResult.Critical : randomNumber >= 5 ? RollResult.Hit : RollResult.Miss;
    }

    rollDX(x: number): number {
        return Math.floor(Math.random() * x) + 1;
    }

    combine(...args: RollResult[]): RollResult {
        let hitStatus = RollResult.Miss;
        for (let i = 0; i < args.length; i++) {
            if (args[i] == RollResult.Critical)
                return RollResult.Critical;
            if (args[i] == RollResult.Hit)
                hitStatus = RollResult.Hit;
        }
        return hitStatus;
    }
}

class RollOutput {
    inspiration() {
        $("#output").append("<br/>");
        $("#output").append(new Roller().rollDX(20));
    }

    activation() {
        var roller = new Roller();
        let str = "";
        for (var i = 0; i < 4; i++) {
            str += roller.rollDX(6);
            if (i != 3)
                str += ", ";
        }
        $("#output").append("<br/>");
        $("#output").append(str);
    }

    

    roll(count: number, colour: Colour) {
        var roller = new Roller();
        let str = "";
        for (var i = 0; i < count; i++) {
            switch (colour) {
                case Colour.Grey:
                    str += RollResult[roller.rollGrey()];
                    break;
                case Colour.Blue:
                    str += RollResult[roller.rollBlue()];
                    break;
                case Colour.Purple:
                    str += RollResult[roller.rollPurple()];
                    break;

            }
            if(i == 0 && count > 1)
            str += " and a ";
        }
        $("#output").append("<br/>");
        $("#output").append(str);
    } 
}
enum RollResult {
    Miss,
    Hit,
    Critical,
    MoveUp
}
enum Colour {
    Grey = "Grey",
    Purple = "Purple",
    Blue = "Blue"
}
enum Weapon {
    Flamer,
    Lasgun ,
    PistolMelee,
    Grenade
}
enum ModelStatus {
    Hidden = 0,
    Engaged = 1,
    InCover = 2,
    Close = 3,
    Other = 4
}
enum Behaviour {
    Hold,
    FallBack,
    Aim,
    Advance,
    Sneak,
    Onslaught,
    Charge,
    Rush,
    Fury
}

class EnemyCarousel {
    enemies: Array<IEnemy>;
    weapons: Array<Array<string>>;
    statuses: Array<ModelStatus>;

    enemyIndex = 0;
    weaponIndex = 0;
    statusIndex = 0;

    constructor() {
        this.enemies = [new TraitorGuardsman()];
        this.weapons = [];
        for (let i = 0; i < this.enemies.length; i++)
            this.weapons.push(this.enemies[i].Weapons());
        this.statuses = this.enemies[this.enemyIndex].States(this.weapons[this.enemyIndex][this.weaponIndex]);
    }

    calculate() :Behaviour {
        return this.enemies[this.enemyIndex].Behave(this.statuses[this.statusIndex]);
    }

    adjustEnemy(direction:Direction) {
        if (direction == Direction.Forward)
            this.enemyIndex = (this.enemyIndex + 1) % this.enemies.length;
        else {
            if (this.enemyIndex == 0)
                this.enemyIndex = this.enemies.length - 1;
            else
                this.enemyIndex--;
        }
    }

    adjustWeapon(direction: Direction) {
        if (direction == Direction.Forward)
            this.weaponIndex = (this.weaponIndex + 1) % this.weapons[this.enemyIndex].length;
        else {
            if (this.weaponIndex == 0)
                this.weaponIndex = this.weapons[this.enemyIndex].length - 1;
            else
                this.weaponIndex--;
        }
        this.getNewStates();

    }

    adjustState(direction: Direction) {
        if (direction == Direction.Forward)
            this.statusIndex = (this.statusIndex + 1) % this.statuses.length;
        else {
            if (this.statusIndex == 0)
                this.statusIndex = this.statuses.length - 1;
            else
                this.statusIndex--;
        }
    }

    private getNewStates() {
        let states = this.enemies[this.enemyIndex].States(this.weapons[this.enemyIndex][this.weaponIndex]);
        let matchingIndex = 0;
        let fullMatch = true;
        for (let i = 0; i < states.length; i++) {
            if (states[i] != this.statuses[i])
                fullMatch = false;
            if (this.statuses[this.statusIndex] == states[i])
                matchingIndex = i;
        }

        if (!fullMatch) {
            this.statusIndex = matchingIndex;
            this.statuses = states;
        }
    }
}

enum Direction {
    Forward,
    Reverse
}

enum EnemyType
{
    TraitorGuardsman  = "Traitor Guardsman"
}
class TraitorGuardsman implements IEnemy {

    Name = EnemyType.TraitorGuardsman;

    private status: ModelStatus;
    private BehaviourTable: Behaviour[][] = 
        [
            [Behaviour.Hold, Behaviour.FallBack, Behaviour.Aim, Behaviour.FallBack, Behaviour.Advance], //1 -3
            [Behaviour.Sneak, Behaviour.Onslaught, Behaviour.Aim, Behaviour.Onslaught, Behaviour.Advance], //4 -6
            [Behaviour.Advance, Behaviour.Onslaught, Behaviour.Aim, Behaviour.Onslaught, Behaviour.Aim],  //7-9
            [Behaviour.Advance, Behaviour.Onslaught, Behaviour.Onslaught, Behaviour.Charge, Behaviour.Aim], //10-12
            [Behaviour.Charge, Behaviour.Onslaught, Behaviour.Onslaught, Behaviour.Charge, Behaviour.Onslaught], //13-15
            [Behaviour.Charge, Behaviour.Onslaught, Behaviour.Onslaught, Behaviour.Charge, Behaviour.Onslaught], //16-19
            [Behaviour.Rush, Behaviour.Fury, Behaviour.Fury, Behaviour.Fury, Behaviour.Rush], //20
        ]
    
    Roll(weapon : Weapon, range : number): RollResult {
        let roller = new Roller();
        switch (weapon) {
            case Weapon.Flamer:
                return range >= 4 ? RollResult.MoveUp : roller.rollBlue();
            case Weapon.Grenade:
                return range == 1 ? roller.rollGrey() : roller.rollPurple();
            case Weapon.Lasgun:
                return roller.rollPurple();
            case Weapon.PistolMelee:
                return range == 1 ? roller.combine(roller.rollPurple(), roller.rollPurple()) : roller.rollGrey();
        }
    }

    Behave(status : ModelStatus) : Behaviour {
        var roll = new Roller().rollDX(20);
        return this.BehaviourTable[this.Map(roll)][status];
    }

    private Map(roll: number): number {
        return roll == 20 ? 6 : roll == 19 ? 5 : Math.floor((roll - 1) / 3); 
    }

    Weapons(): Array<string> {
        return [Weapon[Weapon.Flamer], Weapon[Weapon.Lasgun], Weapon[Weapon.PistolMelee], Weapon[Weapon.Grenade]];
    }

    States(weapon: string): Array<ModelStatus>{
        if (weapon == Weapon[Weapon.Lasgun])
            return [ModelStatus.Hidden, ModelStatus.Engaged, ModelStatus.InCover, ModelStatus.Close, ModelStatus.Other];
        return [ModelStatus.Hidden, ModelStatus.Engaged,  ModelStatus.Close, ModelStatus.Other];
    }
}

interface IEnemy {
    Behave(status: ModelStatus): Behaviour;
    Roll(weapon : Weapon, range: number): RollResult;
    Name: EnemyType;
    Weapons(): Array<string>;
    States(weapon: string): Array<ModelStatus>;
}