class RollBridge {
    init() {
        var roller = new RollOutput();
        $("#r1g").click(function () { roller.roll(1, Colour.Grey); });
        $("#r2g").click(function () { roller.roll(2, Colour.Grey); });
        $("#r1b").click(function () { roller.roll(1, Colour.Blue); });
        $("#r2b").click(function () { roller.roll(2, Colour.Blue); });
        $("#r1p").click(function () { roller.roll(1, Colour.Purple); });
        $("#r2p").click(function () { roller.roll(2, Colour.Purple); });
        $("#ri").click(function () { roller.inspiration(); });
        $("#ra").click(function () { roller.activation(); });
        $("#cls").click(function () { $("#output").html(""); });
        $(".clicker").mousedown(function (e) { e.target.classList.add("pressed"); });
        $(".clicker").mouseup(function (e) { e.target.classList.remove("pressed"); });
        var carousel = new EnemyCarousel();
        this.populateEnemyCarousel(carousel.enemies, "enemyTypeCarouselInner");
        this.populateCarousel(carousel.weapons[carousel.enemyIndex], "enemySubTypeInner");
        this.populateCarousel(carousel.statuses, "enemyBehaviourInner");
        let $this = this;
        $("#enemyType").find(".carousel-control").click(function (e) { carousel.adjustEnemy(e.currentTarget.classList.contains("right") ? Direction.Forward : Direction.Reverse); });
        $("#enemySubType").find(".carousel-control").click(function (e) {
            let direction = e.currentTarget.classList.contains("right") ? Direction.Forward : Direction.Reverse;
            carousel.adjustWeapon(direction);
            $("#enemyBehaviourInner")[0].innerHTML = "";
            $this.populateCarousel(carousel.statuses, "enemyBehaviourInner", carousel.statusIndex);
        });
        $("#enemyBehaviour").find(".carousel-control").click(function (e) { carousel.adjustState(e.currentTarget.classList.contains("right") ? Direction.Forward : Direction.Reverse); });
        $("#enemyResult").click(function (e) { var result = carousel.calculate(); $("#enemyResult")[0].innerHTML = Behaviour[result]; });
    }
    populateEnemyCarousel(list, wrapperId, active = 0) {
        for (let i = 0; i < list.length; i++) {
            let item = this.createCarouselItem(active == i, list[i].Name.toString());
            $(`#${wrapperId}`).append(item);
        }
    }
    populateCarousel(list, wrapperId, active = 0) {
        for (let i = 0; i < list.length; i++) {
            let name = "";
            if (typeof list[i] === "string")
                name = list[i].toString();
            else
                name = ModelStatus[list[i]];
            let item = this.createCarouselItem(active == i, name);
            $(`#${wrapperId}`).append(item);
        }
    }
    isEnemy(item) {
        return item.Name !== undefined;
    }
    createCarouselItem(first, itemText) {
        let cl = first ? "active" : "";
        let item = `<div class="item ${cl} carousel-text"><div style="margin-top:5%">${itemText}</div></div>`;
        return item;
    }
}
//# sourceMappingURL=rollBridge.js.map