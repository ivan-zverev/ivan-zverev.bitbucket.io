class Roller {
    rollGrey() {
        var randomNumber = Math.floor(Math.random() * 6) + 1;
        return randomNumber == 6 ? RollResult.Critical : randomNumber == 5 ? RollResult.Hit : RollResult.Miss;
    }
    rollPurple() {
        var randomNumber = Math.floor(Math.random() * 8) + 1;
        return randomNumber >= 7 ? RollResult.Critical : randomNumber >= 5 ? RollResult.Hit : RollResult.Miss;
    }
    rollBlue() {
        var randomNumber = Math.floor(Math.random() * 12) + 1;
        return randomNumber >= 9 ? RollResult.Critical : randomNumber >= 5 ? RollResult.Hit : RollResult.Miss;
    }
    rollDX(x) {
        return Math.floor(Math.random() * x) + 1;
    }
    combine(...args) {
        let hitStatus = RollResult.Miss;
        for (let i = 0; i < args.length; i++) {
            if (args[i] == RollResult.Critical)
                return RollResult.Critical;
            if (args[i] == RollResult.Hit)
                hitStatus = RollResult.Hit;
        }
        return hitStatus;
    }
}
class RollOutput {
    inspiration() {
        $("#output").append("<br/>");
        $("#output").append(new Roller().rollDX(20));
    }
    activation() {
        var roller = new Roller();
        let str = "";
        for (var i = 0; i < 4; i++) {
            str += roller.rollDX(6);
            if (i != 3)
                str += ", ";
        }
        $("#output").append("<br/>");
        $("#output").append(str);
    }
    roll(count, colour) {
        var roller = new Roller();
        let str = "";
        for (var i = 0; i < count; i++) {
            switch (colour) {
                case Colour.Grey:
                    str += RollResult[roller.rollGrey()];
                    break;
                case Colour.Blue:
                    str += RollResult[roller.rollBlue()];
                    break;
                case Colour.Purple:
                    str += RollResult[roller.rollPurple()];
                    break;
            }
            if (i == 0 && count > 1)
                str += " and a ";
        }
        $("#output").append("<br/>");
        $("#output").append(str);
    }
}
var RollResult;
(function (RollResult) {
    RollResult[RollResult["Miss"] = 0] = "Miss";
    RollResult[RollResult["Hit"] = 1] = "Hit";
    RollResult[RollResult["Critical"] = 2] = "Critical";
    RollResult[RollResult["MoveUp"] = 3] = "MoveUp";
})(RollResult || (RollResult = {}));
var Colour;
(function (Colour) {
    Colour["Grey"] = "Grey";
    Colour["Purple"] = "Purple";
    Colour["Blue"] = "Blue";
})(Colour || (Colour = {}));
var Weapon;
(function (Weapon) {
    Weapon[Weapon["Flamer"] = 0] = "Flamer";
    Weapon[Weapon["Lasgun"] = 1] = "Lasgun";
    Weapon[Weapon["PistolMelee"] = 2] = "PistolMelee";
    Weapon[Weapon["Grenade"] = 3] = "Grenade";
})(Weapon || (Weapon = {}));
var ModelStatus;
(function (ModelStatus) {
    ModelStatus[ModelStatus["Hidden"] = 0] = "Hidden";
    ModelStatus[ModelStatus["Engaged"] = 1] = "Engaged";
    ModelStatus[ModelStatus["InCover"] = 2] = "InCover";
    ModelStatus[ModelStatus["Close"] = 3] = "Close";
    ModelStatus[ModelStatus["Other"] = 4] = "Other";
})(ModelStatus || (ModelStatus = {}));
var Behaviour;
(function (Behaviour) {
    Behaviour[Behaviour["Hold"] = 0] = "Hold";
    Behaviour[Behaviour["FallBack"] = 1] = "FallBack";
    Behaviour[Behaviour["Aim"] = 2] = "Aim";
    Behaviour[Behaviour["Advance"] = 3] = "Advance";
    Behaviour[Behaviour["Sneak"] = 4] = "Sneak";
    Behaviour[Behaviour["Onslaught"] = 5] = "Onslaught";
    Behaviour[Behaviour["Charge"] = 6] = "Charge";
    Behaviour[Behaviour["Rush"] = 7] = "Rush";
    Behaviour[Behaviour["Fury"] = 8] = "Fury";
})(Behaviour || (Behaviour = {}));
class EnemyCarousel {
    constructor() {
        this.enemyIndex = 0;
        this.weaponIndex = 0;
        this.statusIndex = 0;
        this.enemies = [new TraitorGuardsman()];
        this.weapons = [];
        for (let i = 0; i < this.enemies.length; i++)
            this.weapons.push(this.enemies[i].Weapons());
        this.statuses = this.enemies[this.enemyIndex].States(this.weapons[this.enemyIndex][this.weaponIndex]);
    }
    calculate() {
        return this.enemies[this.enemyIndex].Behave(this.statuses[this.statusIndex]);
    }
    adjustEnemy(direction) {
        if (direction == Direction.Forward)
            this.enemyIndex = (this.enemyIndex + 1) % this.enemies.length;
        else {
            if (this.enemyIndex == 0)
                this.enemyIndex = this.enemies.length - 1;
            else
                this.enemyIndex--;
        }
    }
    adjustWeapon(direction) {
        if (direction == Direction.Forward)
            this.weaponIndex = (this.weaponIndex + 1) % this.weapons[this.enemyIndex].length;
        else {
            if (this.weaponIndex == 0)
                this.weaponIndex = this.weapons[this.enemyIndex].length - 1;
            else
                this.weaponIndex--;
        }
        this.getNewStates();
    }
    adjustState(direction) {
        if (direction == Direction.Forward)
            this.statusIndex = (this.statusIndex + 1) % this.statuses.length;
        else {
            if (this.statusIndex == 0)
                this.statusIndex = this.statuses.length - 1;
            else
                this.statusIndex--;
        }
    }
    getNewStates() {
        let states = this.enemies[this.enemyIndex].States(this.weapons[this.enemyIndex][this.weaponIndex]);
        let matchingIndex = 0;
        let fullMatch = true;
        for (let i = 0; i < states.length; i++) {
            if (states[i] != this.statuses[i])
                fullMatch = false;
            if (this.statuses[this.statusIndex] == states[i])
                matchingIndex = i;
        }
        if (!fullMatch) {
            this.statusIndex = matchingIndex;
            this.statuses = states;
        }
    }
}
var Direction;
(function (Direction) {
    Direction[Direction["Forward"] = 0] = "Forward";
    Direction[Direction["Reverse"] = 1] = "Reverse";
})(Direction || (Direction = {}));
var EnemyType;
(function (EnemyType) {
    EnemyType["TraitorGuardsman"] = "Traitor Guardsman";
})(EnemyType || (EnemyType = {}));
class TraitorGuardsman {
    constructor() {
        this.Name = EnemyType.TraitorGuardsman;
        this.BehaviourTable = [
            [Behaviour.Hold, Behaviour.FallBack, Behaviour.Aim, Behaviour.FallBack, Behaviour.Advance],
            [Behaviour.Sneak, Behaviour.Onslaught, Behaviour.Aim, Behaviour.Onslaught, Behaviour.Advance],
            [Behaviour.Advance, Behaviour.Onslaught, Behaviour.Aim, Behaviour.Onslaught, Behaviour.Aim],
            [Behaviour.Advance, Behaviour.Onslaught, Behaviour.Onslaught, Behaviour.Charge, Behaviour.Aim],
            [Behaviour.Charge, Behaviour.Onslaught, Behaviour.Onslaught, Behaviour.Charge, Behaviour.Onslaught],
            [Behaviour.Charge, Behaviour.Onslaught, Behaviour.Onslaught, Behaviour.Charge, Behaviour.Onslaught],
            [Behaviour.Rush, Behaviour.Fury, Behaviour.Fury, Behaviour.Fury, Behaviour.Rush],
        ];
    }
    Roll(weapon, range) {
        let roller = new Roller();
        switch (weapon) {
            case Weapon.Flamer:
                return range >= 4 ? RollResult.MoveUp : roller.rollBlue();
            case Weapon.Grenade:
                return range == 1 ? roller.rollGrey() : roller.rollPurple();
            case Weapon.Lasgun:
                return roller.rollPurple();
            case Weapon.PistolMelee:
                return range == 1 ? roller.combine(roller.rollPurple(), roller.rollPurple()) : roller.rollGrey();
        }
    }
    Behave(status) {
        var roll = new Roller().rollDX(20);
        return this.BehaviourTable[this.Map(roll)][status];
    }
    Map(roll) {
        return roll == 20 ? 6 : roll == 19 ? 5 : Math.floor((roll - 1) / 3);
    }
    Weapons() {
        return [Weapon[Weapon.Flamer], Weapon[Weapon.Lasgun], Weapon[Weapon.PistolMelee], Weapon[Weapon.Grenade]];
    }
    States(weapon) {
        if (weapon == Weapon[Weapon.Lasgun])
            return [ModelStatus.Hidden, ModelStatus.Engaged, ModelStatus.InCover, ModelStatus.Close, ModelStatus.Other];
        return [ModelStatus.Hidden, ModelStatus.Engaged, ModelStatus.Close, ModelStatus.Other];
    }
}
//# sourceMappingURL=rolls.js.map